#!/bin/bash
if [ -z "$BASH_VERSION" ]; then exec bash "$0" "$@" \; exit; fi
export IMAGE_TAG
export DB_USER
export MASTER_BASE_PORT
export MASTER_HTTP_PORT
export DEVSERVER_BASE_PORT
export DEVSERVER_HTTP_PORT
export SERVER1_BASE_PORT
export SERVER1_HTTP_PORT


printf "\n\n"
echo "####################################################"
echo "#                                                  #"
echo "#                                                  #"
echo "#              start po_container.sh               #"
echo "#                                                  #"
echo "#                                                  #"
echo "####################################################"

#### image tag ####
printf "\n\n"
printf "Enter the Patch Date (Image Tag)   "
printf "\n"
read -p "ex)20220819-01                   : " IMAGE_TAG
printf "IMAGE TAG is \033[34m$IMAGE_TAG\033[0m"

printf "\n"
docker images > /home/public/docker_builder/po21_image/config/image_result.txt
export IMAGE_CHECK=`grep -o ${IMAGE_TAG} /home/public/docker_builder/po21_image/config/image_result.txt | wc -w`
if [[ ${IMAGE_CHECK} == 0 ]];
then
   echo "Not found image with this name."
   echo "Container build Failure"
 exit 9
 fi

#### container name ####
printf "\n\n"
printf "Enter the Container Name (Host)   "
printf "\n"
read -p "ex)yj                   : " DB_USER
export CONTAINER_NAME=${IMAGE_TAG}-${DB_USER}
printf "CONTAINER NAME is \033[34mpo21_$CONTAINER_NAME\033[0m"

printf "\n"
docker ps -a > /home/public/docker_builder/po21_image/config/container_result.txt
export CONTAINER_CHECK=`grep -o ${CONTAINER_NAME} /home/public/docker_builder/po21_image/config/container_result.txt | wc -w`
if [[ ${CONTAINER_CHECK} > 0 ]];
then
   echo "You already have a container with the same name"
   echo "Container build Failure"
 exit 9
 fi


#### jeus master base port ####
printf "\n\n"
read -p "Enter the MasterServer Base Port : " MASTER_BASE_PORT
printf "MasterServer Base Port is \033[34m$MASTER_BASE_PORT\033[0m"


#### jeus master http port ####
printf "\n\n"
read -p "Enter the MasterServer Http Port : " MASTER_HTTP_PORT
printf "MasterServer Http Port is \033[34m$MASTER_HTTP_PORT\033[0m"
printf "\n\n"


#### jeus devServer base port ####
printf "\n\n"
read -p "Enter the devServer Base Port : " DEVSERVER_BASE_PORT
printf "devServer Base Port is \033[34m$DEVSERVER_BASE_PORT\033[0m"


#### jeus devServer http port ####
printf "\n\n"
read -p "Enter the devServer Http Port : " DEVSERVER_HTTP_PORT
printf "devServer Http Port is \033[34m$DEVSERVER_HTTP_PORT\033[0m"
printf "\n\n"


#### jeus server1 base port ####
printf "\n\n"
read -p "Enter the server1 Base Port : " SERVER1_BASE_PORT
printf "server1 Base Port is \033[34m$SERVER1_BASE_PORT\033[0m"


#### jeus server1 http port ####
printf "\n\n"
read -p "Enter the server1 Http Port : " SERVER1_HTTP_PORT
printf "server1 Http Port is \033[34m$SERVER1_HTTP_PORT\033[0m"
printf "\n\n"

export DEVSERVER_SPARE_PORT=$((${DEVSERVER_BASE_PORT}+10))
export SERVER1_SPARE_PORT=$((${SERVER1_BASE_PORT}+10))
export MASTER_SPARE_PORT=$((${MASTER_BASE_PORT}+10))

printf "MASTER_SPARE_PORT is \033[34m$MASTER_SPARE_PORT\033[0m\n"
printf "SERVER1_SPARE_PORT is \033[34m$SERVER1_SPARE_PORT\033[0m\n"
printf "DEVSERVER_SPARE_PORT is \033[34m$DEVSERVER_SPARE_PORT\033[0m\n"

printf "\n\n"
echo "####################################################"
echo "#                                                  #"
echo "#                                                  #"
echo "#             po_container_building...             #"
echo "#                                                  #"
echo "#                                                  #"
echo "####################################################"


docker run -idt --network mybridge1 -e MASTER_BASE_PORT=${MASTER_BASE_PORT} -e MASTER_HTTP_PORT=${MASTER_HTTP_PORT} -e DEVSERVER_BASE_PORT=${DEVSERVER_BASE_PORT} -e DEVSERVER_HTTP_PORT=${DEVSERVER_HTTP_PORT} -e SERVER1_BASE_PORT=${SERVER1_BASE_PORT} -e SERVER1_HTTP_PORT=${SERVER1_HTTP_PORT} -e DB_USER=${DB_USER} -e DEVSERVER_SPARE_PORT=${DEVSERVER_SPARE_PORT} -e SERVER1_SPARE_PORT=${SERVER1_SPARE_PORT} -e MASTER_SPARE_PORT=${MASTER_SPARE_PORT} -e LANGUAGE=ko_KR.UTF-8 -e LANG=ko_KR.UTF-8 -h po21${CONTAINER_NAME} -v /etc/localtime:/etc/localtime:ro -e TZ=Asia/Seoul --name po21_${CONTAINER_NAME} -p ${MASTER_BASE_PORT}:${MASTER_BASE_PORT} -p ${MASTER_HTTP_PORT}:${MASTER_HTTP_PORT} -p ${DEVSERVER_BASE_PORT}:${DEVSERVER_BASE_PORT} -p ${DEVSERVER_HTTP_PORT}:${DEVSERVER_HTTP_PORT} -p ${SERVER1_BASE_PORT}:${SERVER1_BASE_PORT} -p ${SERVER1_HTTP_PORT}:${SERVER1_HTTP

