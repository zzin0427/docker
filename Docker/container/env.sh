#!/bin/bash

if [ -z "$BASH_VERSION" ]; then exec bash "$0" "$@" \; exit; fi

export PATH="$JEUS_HOME/bin:$JEUS_HOME/lib:$PATH"
export DOMAIN_ID=$((`expr $(($RANDOM% 100000))""$(($RANDOM% 10000))`+1000000000))
export HOST_NAME=`hostname`

#### change host ####
find /root/jeus21/* -type f -name "*" -exec perl -pi -e "s/localhost/${HOST_NAME}/g" {} \;
find /root/jeus21/bin/startManagedServer -type f -name "*" -exec perl -pi -e "s/9736/${MASTER_BASE_PORT}/g" {} \;
find /root/jeus21/setup/domain-config-template.properties -type f -name "*" -exec perl -pi -e "s/9736/${MASTER_BASE_PORT}/g" {} \;

#### domain.xml ID #####
if [ -z $DOMAIN_ID ]; then export DOMAIN_ID=1165732143; fi
sed -i "s/%DOMAIN_ID%/${DOMAIN_ID}/g" ${DOMAIN_HOME}/config/domain.xml

#### Set JVM-Option #####
export JAVA_VM_PROPERTIES="${JAVA_VM_PROPERTIES} -Xms1024m -Xmx1024m "

#BASE_ADDRESS=`grep "${HOSTNAME}" /etc/hosts | awk '{print $1}'`
if [ -z $BASE_ADDRESS ]; then export BASE_ADDRESS=0.0.0.0; fi
sed -i "s/%BASE_ADDRESS%/${BASE_ADDRESS}/g" ${DOMAIN_HOME}/config/domain.xml

if [ -z $SERVERNAME ]; then export SERVERNAME=`hostname`; fi
sed -i "s/%SERVERNAME%/${SERVERNAME}/g" ${DOMAIN_HOME}/config/domain.xml

#### MASTER_BASE_PORT ####
if [ -z $MASTER_BASE_PORT ]; then export MASTER_BASE_PORT=9736; fi
sed -i "s/%MASTER_BASE_PORT%/${MASTER_BASE_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### MASTER_HTTP_PORT ####
if [ -z $MASTER_HTTP_PORT ]; then export MASTER_HTTP_PORT=8088; fi
sed -i "s/%MASTER_HTTP_PORT%/${MASTER_HTTP_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### DEVSERVER_BASE_PORT ####
if [ -z $DEVSERVER_BASE_PORT ]; then export DEVSERVER_BASE_PORT=8736; fi
sed -i "s/%DEVSERVER_BASE_PORT%/${DEVSERVER_BASE_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### DEVSERVER_HTTP_PORT ####
if [ -z $DEVSERVER_HTTP_PORT ]; then export DEVSERVER_HTTP_PORT=7088; fi
sed -i "s/%DEVSERVER_HTTP_PORT%/${DEVSERVER_HTTP_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### SERVER1_BASE_PORT ####
if [ -z $SERVER1_BASE_PORT ]; then export SERVER1_BASE_PORT=7736; fi
sed -i "s/%SERVER1_BASE_PORT%/${SERVER1_BASE_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### SERVER1_HTTP_PORT ####
if [ -z $SERVER1_HTTP_PORT ]; then export SERVER1_HTTP_PORT=6088; fi
sed -i "s/%SERVER1_HTTP_PORT%/${SERVER1_HTTP_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### DEVSERVER_SPARE_PORT ####
if [ -z $DEVSERVER_SPARE_PORT ]; then export DEVSERVER_SPARE_PORT=6736; fi
sed -i "s/%DEVSERVER_SPARE_PORT%/${DEVSERVER_SPARE_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### SERVER1_SPARE_PORT ####
if [ -z $SERVER1_SPARE_PORT ]; then export SERVER1_SPARE_PORT=5736; fi
sed -i "s/%SERVER1_SPARE_PORT%/${SERVER1_SPARE_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### MASTER_SPARE_PORT ####
if [ -z $MASTER_SPARE_PORT ]; then export MASTER_SPARE_PORT=4736; fi
sed -i "s/%MASTER_SPARE_PORT%/${MASTER_SPARE_PORT}/g" ${DOMAIN_HOME}/config/domain.xml

#### DB_USER ####
if [ -z $DB_USER ]; then export DB_USER=yj; fi
sed -i "s/%DB_USER%/${DB_USER}/g" ${DOMAIN_HOME}/config/domain.xml

#### JEUS admin IP/PW ####
if [ -z $JEUS_USER ]; then export JEUS_USER=jeus; fi
if [ -z $PASSWORD ]; then export PASSWORD=jeus; fi

#### Host Name ####
sed -i "s/localhost/${HOST_NAME}/g" ${SCRIPT_HOME}/setting.sh

#### alias ####
sed -i '84 i\alias dasboot="${JEUS_HOME}/bin/startMasterServer -u '$JEUS_USER' -p '$PASSWORD' -domain domain1"\
alias devboot="${JEUS_HOME}/bin/startManagedServer -domain domain1 -server devServer -u jeus -p jeus"\
alias msboot1="${JEUS_HOME}/bin/startManagedServer -domain domain1 -server server1 -u jeus -p jeus"\
alias dasdown="${JEUS_HOME}/bin/stopServer -host '${HOST_NAME}':'$MASTER_BASE_PORT' -u '$JEUS_USER' -p '$PASSWORD'"\
alias devdown="${JEUS_HOME}/bin/stopServer -host '${HOST_NAME}':'$DEVSERVER_BASE_PORT' -u '$JEUS_USER' -p '$PASSWORD'"\
alias msdown1="${JEUS_HOME}/bin/stopServer -host '${HOST_NAME}':'$SERVER1_BASE_PORT' -u '$JEUS_USER' -p '$PASSWORD'"\
alias qq="msdown1;msboot1"\
alias qqq="msdown1; devdown; dasdown; dasboot; devboot; msboot1"\
alias ms1log="cd /root/jeus21/domains/domain1/servers/server1/logs"\
alias daslog="cd /root/jeus21/domains/domain1/servers/adminServer/logs"\
alias devlog="cd /root/jeus21/domains/domain1/servers/devServer/logs"\
alias jcfg="cd /root/jeus21/domains/domain1/config"\
alias polog="cd /root/ProObject21/logs"\
alias tailj="tail -f JeusServer.log"\
alias tailp="tail -f proobject.log"\
' /root/.bashrc
