#!/bin/bash

export HOST_NAME=localhost

#### start servers ####
${JEUS_HOME}/bin/startManagedServer -domain domain1 -server devServer -u jeus -p jeus
${JEUS_HOME}/bin/startManagedServer -domain domain1 -server server1 -u jeus -p jeus

#### install&deploy ####
echo 'install-application /root/app/promanager-21.0.0.0.war -id promanager
 install-application /root/app/proobject-devserver-21.0.0.0.war -id proobject-devserver
deploy-application -ctxp /promanager promanager -servers devServer
deploy-application -ctxp /proobject-devserver proobject-devserver -servers devServer
exit' | jeusadmin -u jeus -p jeus -host ${HOST_NAME} -port ${MASTER_BASE_PORT}
