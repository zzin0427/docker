#!/bin/bash
if [ -z "$BASH_VERSION" ]; then exec bash "$0" "$@" \; exit; fi


#Docker hub backup
printf "\n"
printf "Enter the \033[31mCONTAINER NAME\033[0m that backup to Docker hub ex)po21_\033[31m20221117-yj\033[0m"
read -p " : " CONTAINER_NAME
printf "Container name is \033[34m$CONTAINER_NAME\033[0m\n\n"

docker commit -a "yujin222" po21_$CONTAINER_NAME yujin222/po21:$CONTAINER_NAME

printf "\n\n"
echo "####################################################"
echo "#                                                  #"
echo "#                                                  #"
echo "#              po_container_pushing...             #"
echo "#                                                  #"
echo "#                                                  #"
echo "####################################################"

docker login -u yujin222 -p song9839!

docker push yujin222/po21:$CONTAINER_NAME

docker rmi yujin222/po21:$CONTAINER_NAME
