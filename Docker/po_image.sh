#!/bin/bash
if [ -z "$BASH_VERSION" ]; then exec bash "$0" "$@" \; exit; fi
export IMAGE_TAG

printf "\n\n"
echo "####################################################"
echo "#                                                  #"
echo "#                                                  #"
echo "#                start po21_image.sh               #"
echo "#                                                  #"
echo "#                                                  #"
echo "####################################################"

#### image tag ####
printf "\n\n"
printf "Enter the Patch Date (Image Tag)   "
printf "\n"
read -p "ex)20220819-01                   : " IMAGE_TAG
printf "IMAGE TAG is \033[34m$IMAGE_TAG\033[0m"


printf "\n\n"
echo "####################################################"
echo "#                                                  #"
echo "#                                                  #"
echo "#              po21_image_building...              #"
echo "#                                                  #"
echo "#                                                  #"
echo "####################################################"

docker build -f Dockerfile.jeus_ubuntu --no-cache --force-rm -t yujin222/po21:${IMAGE_TAG} .


#Docker hub backup
printf "backup image(yujin222/po21:${IMAGE_TAG}) to Docker Hub"

docker login -u yujin222 -p song9839!

docker push yujin222/po21:${IMAGE_TAG}
